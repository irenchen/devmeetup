
import Vue from 'vue'

import Vuetify from 'vuetify'
import './stylus/main.styl'

import App from './App'
import router from './router'
import { store } from './store'
import DateFilter from './filters/date'
import Alert from '@/components/shared/Alert'
import EditMeetupDialog from '@/components/meetup/EditMeetupDialog'
import EditMeetupDateDialog from '@/components/meetup/EditMeetupDateDialog'
import EditMeetupTimeDialog from '@/components/meetup/EditMeetupTimeDialog'
import RegisterDialog from '@/components/meetup/RegisterDialog'
import * as firebase from 'firebase'

Vue.use(Vuetify)

Vue.config.productionTip = false

Vue.filter('date', DateFilter)
Vue.component('Alert', Alert)
Vue.component('EditMeetupDialog', EditMeetupDialog)
Vue.component('EditMeetupDateDialog', EditMeetupDateDialog)
Vue.component('EditMeetupTimeDialog', EditMeetupTimeDialog)
Vue.component('RegisterDialog', RegisterDialog)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBvgEWwj9gTwVSCX7FMwGDLqcZF9E_KLZE',
      authDomain: 'devmeetup-49c14.firebaseapp.com',
      databaseURL: 'https://devmeetup-49c14.firebaseio.com',
      projectId: 'devmeetup-49c14',
      storageBucket: 'gs://devmeetup-49c14.appspot.com'
    })
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        // this.$store.dispatch('fetchUserData')
      }
    })
    this.$store.dispatch('loadMeetups')
  }
})
