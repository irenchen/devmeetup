import * as firebase from 'firebase'

export default {
  state: {
    loadedMeetups: [
      {
        imageUrl: 'https://media-cdn.tripadvisor.com/media/photo-s/0e/9a/e3/1d/freedom-tower.jpg',
        id: 'adsfasdgasde123',
        title: 'Meetup in New York',
        location: 'New York Time Square',
        description: 'greate!',
        date: new Date()
      },
      {
        imageUrl: 'http://www.telegraph.co.uk/content/dam/Travel/Destinations/Europe/France/Paris/paris-attractions-xlarge.jpg',
        id: 'jqkewjkdlask123',
        title: 'Meetup in Paris',
        location: 'Paris Tower',
        description: 'wonderful!',
        date: '2017-12-25'
      }
    ]
  },
  mutations: {
    setLoadedMeetups (state, payload) {
      state.loadedMeetups = payload
    },
    createMeetup (state, payload) {
      state.loadedMeetups.push(payload)
    },
    updateMeetup (state, payload) {
      const meetup = state.loadedMeetups.find(m => m.id === payload.id)
      if (payload.title) meetup.title = payload.title
      if (payload.description) meetup.description = payload.description
      if (payload.date) meetup.date = payload.date
    }
  },
  actions: {
    loadMeetups ({ commit }) {
      commit('setAuthState', { loading: true })
      firebase.database().ref('meetups').once('value')
        .then(data => {
          const meetups = []
          const obj = data.val()
          for (let key in obj) {
            meetups.push({
              id: key,
              title: obj[key].title,
              description: obj[key].description,
              location: obj[key].location,
              imageUrl: obj[key].imageUrl,
              date: obj[key].date,
              creatorId: obj[key].creatorId
            })
          }
          commit('setLoadedMeetups', meetups)
          commit('setAuthState', { loading: false })
        })
        .catch(err => {
          console.log(err)
          commit('setAuthState', { loading: false })
        })
    },
    createMeetup ({ commit, getters }, payload) {
      const meetup = {
        title: payload.title,
        location: payload.location,
        description: payload.description,
        date: payload.date.toISOString(),
        creatorId: getters.user.id
      }
      // Reach out to firebase and store it
      let imageUrl
      let key
      firebase.database().ref('meetups').push(meetup)
        .then(data => {
          key = data.key

          return key
        })
        .then(key => {
          const filename = payload.image.name
          const ext = filename.slice(filename.lastIndexOf('.') + 1)
          return firebase.storage().ref('meetups/' + key + '.' + ext).put(payload.image)
        })
        .then(fileData => {
          imageUrl = fileData.metadata.downloadURLs[0]
          return firebase.database().ref('meetups').child(key).update({ imageUrl })
        })
        .then(() => {
          commit('createMeetup', {
            ...meetup,
            id: key,
            imageUrl: imageUrl
          })
        })
        .catch(err => {
          console.log(err)
        })
    },
    updateMeetup ({ commit, getters }, payload) {
      commit('setAuthState', { loading: true })
      const updated = {}
      if (payload.titile) {
        updated.title = payload.title
      }
      if (payload.description) {
        updated.description = payload.description
      }
      if (payload.date) {
        updated.date = payload.date.toISOString()
      }
      firebase.database().ref('meetups').child(payload.id).update(updated)
        .then(() => {
          commit('setAuthState', { loading: false })
          commit('updateMeetup', payload)
        })
        .catch(err => {
          console.log(err)
          commit('setAuthState', { loading: false })
        })
    }
  },
  getters: {
    loadedMeetups (state) {
      return state.loadedMeetups.sort((a, b) => {
        return a.date > b.date
      })
    },
    featuredMeetups (state, getters) {
      return getters.loadedMeetups.slice(0, 5)
    },
    loadedMeetup (state) {
      return (id) => {
        return state.loadedMeetups.find((meetup) => {
          return meetup.id === id
        })
      }
    }
  }
}
