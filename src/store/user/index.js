import * as firebase from 'firebase'

export default {
  state: {
    user: null
  },
  mutations: {
    registerUserForMeetup (state, payload) {
      const mid = payload.id
      if (state.user.registeredMeetups.findIndex(m => m.id === mid) >= 0) {
        return
      }
      state.user.registeredMeetups.push(mid)
      state.user.fbKeys[mid] = payload.fbKey
    },
    unregisterUserFromMeetup (state, payload) {
      const registeredMeetups = state.user.registeredMeetups
      registeredMeetups.splice(registeredMeetups.findIndex(m => m.id === payload), 1)
      Reflect.deleteProperty(state.user.fbKeys, payload)
    },
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    registerUserForMeetup ({ commit, getters }, payload) {
      commit('setAuthState', { loading: true })
      const user = getters.user
      firebase.database().ref('/users/' + user.id).child('/registration/')
        .push(payload)
        .then(data => {
          commit('setAuthState', { loading: false })
          commit('registerUserForMeetup', { id: payload, fbKey: data.key })
        })
        .catch(err => {
          console.log(err)
          commit('setAuthState', { loading: false })
        })
    },
    unregisterUserFromMeetup ({ commit, getters }, payload) {
      commit('setAuthState', { loading: true })
      const user = getters.user
      if (!user.fbKeys) return
      const fbKey = user.fbKeys[payload]
      firebase.database().ref('/users/' + user.id + '/registration/')
        .child(fbKey)
        .remove()
        .then(() => {
          commit('setAuthState', { loading: false })
          commit('unregisterUserFromMeetup', payload)
        })
        .catch(err => {
          console.log(err)
          commit('setAuthState', { loading: false })
        })
    },
    signUserUp ({ commit }, payload) {
      commit('setAuthState', { loading: true })
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(user => {
          commit('setAuthState', { loading: false })
          const newUser = {
            id: user.uid,
            registeredMeetups: [],
            fbKeys: {}
          }
          commit('setUser', newUser)
        })
        .catch(err => {
          commit('setAuthState', { loading: false, error: err.message })
          console.log(err)
        })
    },
    signUserIn ({ commit }, payload) {
      commit('setAuthState', { loading: true })
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(user => {
          commit('setAuthState', { loading: false })
          const newUser = {
            id: user.uid,
            registeredMeetups: [],
            fbKeys: {}
          }
          commit('setUser', newUser)
        })
        .catch(err => {
          commit('setAuthState', { loading: false, error: err.message })
          console.log(err)
        })
    },
    autoSignIn ({ commit }, payload) {
      commit('setUser', {
        id: payload.uid,
        registeredMeetups: [],
        fbKeys: {}
      })
    },
    fetchUserData ({ commit, getters }) {
      commit('setAuthState', { loading: true })
      firebase.database().ref('/users/' + getters.user.id + '/registration')
        .once('value')
        .then(data => {
          const dataPairs = data.val()
          let registeredMeetups = []
          let swappedPairs = {}
          for (let key in dataPairs) {
            registeredMeetups.push(dataPairs[key])
            swappedPairs[dataPairs[key]] = key
          }
          const updatedUser = {
            id: getters.user.id,
            registeredMeetups: registeredMeetups,
            fbKeys: swappedPairs
          }
          commit('setAuthState', { loading: false })
          commit('setUser', updatedUser)
        })
        .catch(err => {
          console.log(err)
          commit('setAuthState', { loading: false })
        })
    },
    logout ({ commit }) {
      firebase.auth().signOut()
      commit('setUser', null)
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
}
