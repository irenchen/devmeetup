export default {
  state: {
    authState: {
      loading: false,
      error: null
    }
  },
  mutations: {
    setAuthState (state, payload) {
      state.authState = {
        ...state.authState,
        loading: payload.loading || false,
        error: payload.error || null
      }
    },
    clearAuthState (state) {
      state.authState = {
        loading: false,
        error: null
      }
    }
  },
  actions: {
    clearAuthState ({ commit }) {
      commit('clearAuthState')
    }
  },
  getters: {
    authState (state) {
      return state.authState
    }
  }
}
