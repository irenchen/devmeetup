# devmeetup

> A dev meetup application

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

### Reference from
#### [youtube video](https://www.youtube.com/watch?v=FXY1UyQfSFw&list=PL55RiY5tL51qxUbODJG9cgrsVd7ZHbPrt&index=1)
#### [vuetifyjs doc](https://vuetifyjs.com/vuetify/quick-start)
#### [google firebase doc]()

#### publish to firebase hosting
npm install -g firebase-cli
firebase --interactive login
mkdir tst
cd tst
firebase init

cp devmeetup/dist/* into tst/public/

firebase deploy
 
